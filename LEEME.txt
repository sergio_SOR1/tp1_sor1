# Sincronizaci�n de cuentas con GitLab  - 1ra forma #
## NOTA: las carpetas "tp_prueba" y "tp_pruebaA" son, justamente, de testeo/prueba. Se pueden eliminar.
## La idea ser�a crear directorios enlazados a alg�n repositorio de GitLab, obtener su path (por ej. "/home/gonza/tp_sorB") y verificar que el s�per men� funcione correctamente (leer m�s abajo).
1) Ingresar al directorio sor1_tp;
2) Ingresar a la carpeta ".git";
3) Ingresar al archivo "config" (abrir con alg�n editor de texto como Notepad);
3a) Modificar la parte que dice "url = https://gitlab.com/gonza_g96/sor1_tp.git
" por el link del repositorio con el que se desea vincular.
# por ej. "url=https://gitlab.com/alan_gaitan/tp_prueba.git" (DEBE ESTAR CREADO EN GITLAB).
4) Ingresar al archivo FETCH_HEAD;
4a) Modificar la linea "branch 'master' of https://gitlab.com/gonza_g96/sor1_tp
" incluyendo el link del repositorio con el que se desea vincular.
# por ej. "branch 'master' of https://gitlab.com/alan_gaitan/tp_prueba
" (DEBE ESTAR CREADO EN GITLAB).

# Sincronizaci�n de cuentas con GitLab - 2da forma #
Si los anteriores pasos no funcionaron, entonces probar los siguientes:
0) Ingresar a la carpeta del TP (en este caso, TP_SOR1 o como sea que se haya renombrado) y eliminar la subcarpeta ".git".
1) Crear el repositorio correspondiente en GitLab;
2) Realizar un GIT CLONE del mismo en el directorio donde se desea guardar el supermenu.sh (por ej, "/home")
# por ej. "git clone https://gitlab.com/gonza_g96/tp_sor1" en /home ||| esto crea una carpeta/dir en /home llamado "tp_sor1".
3) Una vez realizada la clonaci�n, se debe pegar el contenido de la carpeta donde se encuentra el superMenu.sh en el mismo.
# este shell-script necesita permisos de ejecuci�n: utilizar "sudo chmod 777 supermenu.sh" para ejecutarlo.
# recordar modificar la variable proyectoActual del superMenu.sh con la ruta de acceso (path) del directorio donde el mismo se encuentra:
# # por ej. si supermenu.sh esta en "/home/usuario/tp_sor1", entonces proyectoActual = "/home/usuario/tp_sor1";
# # # se puede utilizar cualquier editor de archivos: gedit [gedit supermenu.sh], nano, vi, etc.)
4) Verificar que este s�per men� est� funcionando: leer instrucciones del archivo .sh o seguir los pasos del men� en su parte gr�fica.

--------------------------------------------------------------------------------------------------------------------------

# Para la parte de hilos (threads) #
*) Leer documentaci�n que se encuentra dentro de seccionCritica.c (gedit seccionCritica.c) para visualizar c�mo funciona el sem�foro de acceso a recursos compartidos (mutex);
**) Leer documentaci�n que se encuentra dentro de sugar_tututu.c (gedit sugar_tututu.c) para visualizar c�mo funcionan los sem�foros de acceso a recursos compartidos (mutex) y de sincronizaci�n;
## para compilar los archivos del tipo thread (mencionados arriba): usar "gcc nombreArchivo.c -o nombre -lpthread";
### por ej. si quiero compilar a seccionCritica.c, entonces ... "gcc seccionCritica.c -o nombre seccionCritica -lpthread" (recordar tener permisos de ejecuci�n | sudo chmod 777 seccionCritica.c);
### luego, ejecuto escribiendo "./seccionCritica";
## en ambos casos, las comillas solo est�n con fines descriptivos | se deben suprimir/eliminar ##
