#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	red=`tput setaf 1`;
	green=`tput setaf 2`;
	blue=`tput setaf 4`;
	bg_blue=`tput setab 4`;
	reset=`tput sgr0`;
	bold=`tput setaf bold`;
	white=`tput setaf 7`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------
proyectoActual="/home/sarkiz/Escritorio/TP_SORI/sor1_tp
";
proyectos="/home/sarkiz/Escritorio/TP_SORI/sor1_tp";

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
	
    echo -e "\t El proyecto actual es: $proyectoActual";
    echo -e "\t # Ej. /home/gonza/sor1_tp #";
    echo "";
    echo -e "\t # Si se ejecuta x 1ra vez > cambiar link de proyectos (repos.txt) #";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Guardar cambios";
    echo -e "\t\t\t c.  Actualizar repo";
    echo -e "\t\t\t d.  Cambiar proyecto";        
    echo -e "\t\t\t e.  Agregar proyecto nuevo";        
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t Usuario: $USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${white} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${white}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${white} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "[...]";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
	echo $1;
	while true; do
		echo "desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}

#------------------------------------------------------
# FUNCIONES del MENU
#------------------------------------------------------
a_funcion () {
    	imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
	# se utiliza el comando git status para obtener el estado del proyecto en GitLab
    	decidir "cd $proyectoActual; git status";
}

b_funcion () {
       	imprimir_encabezado "\tOpción b.  Guardar cambios";
	decidir "git add -A";
	# imprimo un mensaje x pantalla para que el usuario ingrese un mensaje
	echo -e "Ingrese el mensaje de su commit ...";
	# leo el mensaje ingresado
	read mensajeIngresado
	# continuo con la ejecucion (teniendo en cuenta el mensaje ingresado como parametro)
	decidir "git commit -m $mensajeIngresado";
	# subo al repositorio en GitLab los cambios efectuados en el directorio local (rama origin/master)
	decidir "git push -u origin master";
}

c_funcion () {
      	imprimir_encabezado "\tOpción c.  Actualizar repo";
	# utilizo git pull para "traer" todos los archivos que se encuentren en la rama origin/master del repositorio GitLab, al directorio local.
	decidir "git pull origin master";
}

d_funcion () {
	imprimir_encabezado "\tOpción d.  Cambiar proyecto";
	echo "## [Mostrando proyectos almacenados] ##";
	cat repos.txt;
	echo "## [Seleccione un proyecto] ##";
	echo "## Ej. '1' (sin comillas) para el primer path ##";
	echo "## Ej. '2' (sin comillas) para el segundo path ##";
	echo "## [NOTA: el path debe tener su repositorio GitLab correspondiente] ##";
	echo "## [NOTA: para configurar el mismo, verificar en la pag. oficial de GitLab] ##";
	# leemos el numero de opcion elegida
	read opcionNumero;
	# leemos la linea correspondiente del path respecto a la opcion que eligio el usuario
	pathSeleccionado=$(head -n $opcionNumero repos.txt | tail -1);
	# asignamos el path correspondiente a la variable global proyectoActual
	proyectoActual=$pathSeleccionado;
	echo "[Cambio realizado con éxito]";
	echo "#################################################################";
	echo "## [Recuerde que el path que eligio debe apuntar a un directorio enlazado con GitLab] ##";
	echo "## [Si ud. no creo ningun directorio aun, dirijase al path principal (ej. '/home') ...] ##";
	echo "## [y escriba 'git clone https://gitlab.com/usuarioReposGitLab/nombreReposEnLaNube'] ##";
	echo "## Ej. añadi el siguiente path (pero aun no cree ningun directorio): '/home/gonza/prueba1' ##";
	echo "## [entonces, me dirijo a '/home' y escribo 'git clone https://gitlab.com/miUsuarioGitLab/prueba1'] ##";
	echo "#################################################################";
	echo "### SI NO REALIZA ESTA ACCION, ESTE SHELL SCRIPT NO FUNCIONARA ADECUADAMENTE ###";
}

e_funcion () {
	imprimir_encabezado "\tOpción e.  Agregar proyecto nuevo";   
	# [mensajes al usuario]
	echo "Ingrese a continuacion el path correspondiente al directorio local ...";
	echo "[Recordar que el nombre del mismo debe coincidir con el repositorio de GitLab]";
	read pathRepo
	# grabamos el path al final del archivo de repos.txt
	echo $pathRepo >> repos.txt  
	echo "[¡Añadido!]";
	echo "#################################################################";
	echo "## [Recuerde que el path que eligio debe apuntar a un directorio enlazado con GitLab] ##";
	echo "## [Si ud. no creo ningun directorio aun, dirijase al path principal (ej. '/home') ...] ##";
	echo "## [y escriba 'git clone https://gitlab.com/usuarioReposGitLab/nombreReposEnLaNube'] ##";
	echo "## Ej. añadi el siguiente path (pero aun no cree ningun directorio): '/home/gonza/prueba1' ##";
	echo "## [entonces, me dirijo a '/home' y escribo 'git clone https://gitlab.com/miUsuarioGitLab/prueba1'] ##";
	echo "#################################################################";
	echo "### SI NO REALIZA ESTA ACCION, ESTE SHELL SCRIPT NO FUNCIONARA ADECUADAMENTE ###";
}


#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done
 
