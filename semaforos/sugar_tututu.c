#include <stdio.h> //standard input/output
#include <pthread.h> //para usar threads

#include <unistd.h>     // para hacer sleep
#include <stdlib.h>     // para libreria de numeros random: srand, rand 
#include <time.h>       // para tomar el tiempo 

// comentarios con '#' realizados con fines descriptivos.
#include <semaphore.h> // para incluir a los semaforos de sincronizacion.
#define SEMAPHORES 4 // cantidad de semaforos.

// declaracion de los semaforos - DOCUMENTACION - LEER.
// # El semaforo (sem1) se utilizara para tocar la primer parte de la melodia: su valor, al inicio, es de 1 (para que se pueda realizr un sem1.wait() | acceder al mismo).
// # una vez que se toco esta parte de la melodia, la funcion tocar_movimiento1() habilita al sem2, para que toque la segunda parte de la musica; se repite el mismo procedimiento que el resumen anterior. Una vez finalizada la parte 2, la funcion tocar_movimiento_2() da señal/habilita al sem3; cuando termina la parte 3, se habilita a sem4 y luego, al final, se habilita nuevamente a sem1.
// # sem1: semaforo de acceso a recurso compartido = VALOR INICIAL 1 (para que sea el primero en ser accedido).
// # sem2, sem3, sem4: semaforos de sincronizacion = VALOR INICIAL 0 (se van habilitando conforme finalice la ejecucion de un proceso en particular).
sem_t sem1;
sem_t sem2;
sem_t sem3;
sem_t sem4;

#define NUM_THREADS 4
//cada funcion tocar_movimiento_i toca una parte de la melodia
void* tocar_movimiento_1 (void* parametro)
{
	while(1){
		// # se accede al semaforo sem1 para tocar la primer parte de la melodia.
		sem_wait(&sem1);
	   	system("echo Start; mplayer -really-quiet part1_SugarSugar.mp3");
		// # una vez se toco la primer parte de la cancion, se da "señal" (signal) al semaforo sem2, para que se toque la segunda parte.
		sem_post(&sem2);
	}   
   	pthread_exit(NULL);
}

void* tocar_movimiento_2 (void* parametro)
{   
	while(1){
		// # se accede al semaforo sem2 para tocar la segunda parte de la melodia.
		sem_wait(&sem2);
	   	system("mplayer -really-quiet part2_SugarSugar.mp3");
		// # una vez se toco la segunda parte, se da signal al semaforo sem3 para tocar la tercer parte de la cancion.
		sem_post(&sem3);
	}   
   	pthread_exit(NULL);
}

void* tocar_movimiento_3 (void* parametro)
{   
	while(1){
		// # se accede al semaforo sem3 para tocar la tercer parte de la cancion.
		sem_wait(&sem3);
	   	system("mplayer -really-quiet part3_SugarSugar.mp3");
		// # una vez se toco la tercer parte, se da signal al semaforo sem4 para tocar la cuarta y ultima parte de la cancion.
		sem_post(&sem4);
	}   
   	pthread_exit(NULL);
}

void* tocar_movimiento_4 (void* parametro)
{   
	while(1){
		// # se accede al semaforo sem4 para tocar la ultima parte (numero 4) de la cancion.
		sem_wait(&sem4);
	   	system("mplayer -really-quiet part4_SugarSugar.mp3");
		// # una vez se toco la cuarta/ultima parte, se da signal al semaforo sem1 para que se toque nuevamente la primer parte.
		// # observar que en este preciso momento, los semaforos sem1, sem2, sem3 y sem4 estan con valor 0. Por lo tanto, se habilita el primer semaforo (sem1) para que, cuando termine de tocar la primera parte de la cancion, se habilite al semaforo (sem2) y, este ultimo, al semaforo (sem3) y, luego, al semaforo (sem4)...
		sem_post(&sem1);
	}   
   	pthread_exit(NULL);
}

int main ()
{
	// # inicializacion de los semaforos #
	sem_init(&sem1, 0, 1); // primer semaforo para la 1ra parte de la cancion: inicializado en 1.
	sem_init(&sem2, 0, 0); // segundo semaforo para la 2da parte de la cancion: inicializado en 0.
	sem_init(&sem3, 0, 0); // tercer semaforo para la 3ra parte de la cancion: inicializado en 0.
	sem_init(&sem4, 0, 0); // cuarto semaforo para la 4ta parte de la cancion: inicializado en 0.

   	pthread_t threads[NUM_THREADS]; //una variable de tipo pthread_t sirve para identificar cada hilo que se cree
                                   //la variable threads es una array de pthread_t
                                   //comparar con char data[100], un array de char                                           
	
	//genero los threads y los lanzo, observar que sin semaforos se ejecutan los 4 casi al mismo tiempo
	//y no se reconoce la melodía   	
	int rc;
   	rc = pthread_create(&threads[1], NULL, tocar_movimiento_2, NULL );
   	rc = pthread_create(&threads[0], NULL, tocar_movimiento_1, NULL );
   	rc = pthread_create(&threads[3], NULL, tocar_movimiento_4, NULL );
   	rc = pthread_create(&threads[2], NULL, tocar_movimiento_3, NULL );
   	

	//esperar a que los threads terminen para terminar el programa principal
	int i;
    	for(i = 0 ; i < NUM_THREADS ; i++)
    	{
        	pthread_join(threads[i] , NULL);
    	}
    	
	pthread_exit(NULL);
	return EXIT_SUCCESS;
}

//Para compilar:   gcc sugar_tututu.c -o sugar_tututu -lpthread
//Para ejecutar:   ./sugar_tututu






